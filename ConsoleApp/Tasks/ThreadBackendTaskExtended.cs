﻿using Microsoft.Extensions.Logging;

namespace ConsoleApp.Tasks;

public class ThreadBackendTaskExtended : ThreadBackendTask
{
    public const int CountOfThreads = 5;
    private SemaphoreSlim _signal = new SemaphoreSlim(CountOfThreads);
    private readonly ILogger<ThreadBackendTask> _logger;

    public ThreadBackendTaskExtended(ILogger<ThreadBackendTask> logger)
    {
        _logger = logger;
    }

    protected override async Task<IEnumerable<ThreadTaskItemResult>> ExecuteAsync(IEnumerable<ThreadTaskItemConfig> configs)
    {
        var tasks = configs.Select(async c =>
        {
            await _signal.WaitAsync();
            try
            {
                return await ExecuteAsync(c);
            }
            finally
            {
                _signal.Release(1);
            }
        });
    
        return await Task.WhenAll(tasks);
    }
    
    protected override void WriteResult(ThreadTaskItemResult result) {
        _logger.LogInformation(result.Message);
    }
}