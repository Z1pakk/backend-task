﻿using ConsoleApp.Models;
using Microsoft.Extensions.Logging;

namespace ConsoleApp.Tasks;

public class ExpressionBackendTaskExtended : ExpressionBackendTask
{
    private readonly ILogger<ExpressionBackendTask> _logger;

    public ExpressionBackendTaskExtended(ILogger<ExpressionBackendTask> logger)
    {
        _logger = logger;
    }

    protected override async Task<IQueryable<T>> LoadRecords<T>(params string[] fields)
    {
        var records = (await base.LoadRecords<T>());
        foreach (var record in records)
        {
            Type recordType = record.GetType();
            foreach (var propertyInfo in recordType.GetProperties())
            {
                if (!fields.Contains(propertyInfo.Name))
                {
                    if (propertyInfo.PropertyType == typeof(string))
                    {
                        propertyInfo.SetValue(record, Convert.ChangeType(null, propertyInfo.PropertyType), null);
                    }
                    if (propertyInfo.PropertyType == typeof(decimal))
                    {
                        propertyInfo.SetValue(record, Convert.ChangeType(0, propertyInfo.PropertyType), null);
                    }
                }
            }
        }
        return records;
    }

    protected override void WriteRecord<T>(T record, params string[] fields) {
        var fieldMessages = fields.Select(field => $"{field} = {GetValueByProperty(record, field)}");
        this._logger.LogInformation($"Record {record.Id}: \n{string.Join("; ", fieldMessages)}");
    }
    
    private string GetValueByProperty<T>(T record, string field) where T : IPrimaryEntity
    {
        return record.GetType()?.GetProperty(field)?.GetValue(record)?.ToString();
    }
}