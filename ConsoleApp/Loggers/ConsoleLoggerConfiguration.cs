﻿using Microsoft.Extensions.Logging;

namespace ConsoleApp.Providers;

public sealed class ConsoleLoggerConfiguration
{
    public int EventId { get; set; }

    public Dictionary<LogLevel, ConsoleColor> LogLevelToColorMap { get; set; } = new()
    {
        [LogLevel.Information] = ConsoleColor.Green,
        [LogLevel.Warning] = ConsoleColor.DarkCyan,
        [LogLevel.Error] = ConsoleColor.DarkRed
    };
}