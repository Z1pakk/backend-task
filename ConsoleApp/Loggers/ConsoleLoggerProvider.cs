﻿using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;

namespace ConsoleApp.Providers;

public sealed class ConsoleLoggerProvider : ILoggerProvider
{
    private readonly ConcurrentDictionary<string, ConsoleLogger> _loggers =
        new(StringComparer.OrdinalIgnoreCase);

    public ILogger CreateLogger(string categoryName) =>
        _loggers.GetOrAdd(categoryName, name => new ConsoleLogger(name, GetCurrentConfig));

    private ConsoleLoggerConfiguration GetCurrentConfig() => new ConsoleLoggerConfiguration();

    public void Dispose()
    {
        _loggers.Clear();
    }
}