﻿using Microsoft.Extensions.Logging;

namespace ConsoleApp.Providers;

public sealed class ConsoleLogger(
    string name,
    Func<ConsoleLoggerConfiguration> getCurrentConfig) : ILogger
{
    public bool IsEnabled(LogLevel logLevel) =>
        getCurrentConfig().LogLevelToColorMap.ContainsKey(logLevel);

    public IDisposable? BeginScope<TState>(TState state) where TState : notnull => default!;
    
    public void Log<TState>(
        LogLevel logLevel,
        EventId eventId,
        TState state,
        Exception? exception,
        Func<TState, Exception?, string> formatter)
    {
        if (!IsEnabled(logLevel))
        {
            return;
        }

        ConsoleLoggerConfiguration config = getCurrentConfig();
        if (config.EventId == 0 || config.EventId == eventId.Id)
        {
            ConsoleColor originalColor = Console.ForegroundColor;

            Console.ForegroundColor = config.LogLevelToColorMap[logLevel];
            Console.Write($"[{eventId.Id,2}: {logLevel,-12}]");
            
            Console.ForegroundColor = originalColor;
            Console.WriteLine($" {name}:");

            Console.ForegroundColor = originalColor;
            Console.Write($"{formatter(state, exception)}\n\n");
        }
    }
}