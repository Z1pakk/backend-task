﻿using ConsoleApp.Providers;
using ConsoleApp.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace ConsoleApp;

public static class ServiceCollection
{
    public static void RegisterServices(this IServiceCollection services)
    {
        services.AddLogging(options =>
        {
            options.AddProvider(new ConsoleLoggerProvider());
        });
        
        services.AddTransient<ThreadBackendTask, ThreadBackendTaskExtended>();
        services.AddTransient<ExpressionBackendTask, ExpressionBackendTaskExtended>();
    }
}