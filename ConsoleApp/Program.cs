﻿using ConsoleApp;
using ConsoleApp.Tasks;
using Microsoft.Extensions.DependencyInjection;
using ServiceCollection = Microsoft.Extensions.DependencyInjection.ServiceCollection;

IServiceProvider CreateServices()
{
    var services = new ServiceCollection();
    services.RegisterServices();
    return services.BuildServiceProvider();
}
var services = CreateServices();

var task1 = services.GetRequiredService<ThreadBackendTask>();
await task1.RunAsync();

var task2 = services.GetRequiredService<ExpressionBackendTask>();
await task2.RunAsync();

